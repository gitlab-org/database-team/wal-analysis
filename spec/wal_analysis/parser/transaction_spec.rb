require 'spec_helper'

RSpec.describe WalAnalysis::Parser::Transaction do

  context 'with a commit without extra information' do
    let(:raw) do
      <<~RAW.gsub(/\n/, ' ').strip
        rmgr: Transaction len (rec/tot):     46/    46,
        tx: 2200386854, lsn: DB652/159B5690, prev DB652/159B3858,
        desc: COMMIT 2023-12-10 00:59:40.401732 UTC
      RAW
    end

    it 'parses' do
      expect(described_class.parse(raw)).to be_a(WalAnalysis::Parser::Commit)
    end
  end



  context 'with a commit with extra information' do
    let(:raw) do
      <<~RAW.gsub(/\n/, ' ').strip
        rmgr: Transaction len (rec/tot):     46/    46,
        tx: 2200386854, lsn: DB652/159B5690, prev DB652/159B3858,
        desc: COMMIT 2023-12-10 00:59:40.401732 UTC;
        inval msgs: catcache 59 catcache 51 catcache 50 catcache 51 catcache 50 relcache 255848970 relcache 25584897
      RAW
    end

    it 'parses' do
      expect(described_class.parse(raw)).to be_a(WalAnalysis::Parser::Commit)
    end
  end

  context 'with an invalidation' do
    let(:raw) do
      <<~RAW.gsub(/\n/, ' ').strip
        rmgr: Transaction len (rec/tot):    174/   174, tx:          0,
        lsn: DB652/186D7BE8, prev DB652/186D7AC0,
        desc: INVALIDATION ;
        inval msgs: catcache 51 catcache 50 catcache 51 catcache 50 catcache 51 catcache 50
                    relcache 255848970 relcache 255848977 relcache 25584896
      RAW
    end


    it 'parses as an Unparsed record' do
      expect(described_class.parse(raw)).to be_a(WalAnalysis::Parser::Unparsed)
    end
  end
end
