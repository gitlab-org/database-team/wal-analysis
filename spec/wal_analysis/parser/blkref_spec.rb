require 'spec_helper'

RSpec.describe WalAnalysis::Parser::BlkRef do
  let(:raw) { 'blkref #0: rel 111/222/333 blk 444'}

  it 'parses' do
    expected_location = WalAnalysis::Location.new(tablespace_oid: 111, database_oid: 222, relfilenode: 333)
    expect(described_class.parse(raw))
      .to eq(described_class.new(location: expected_location, block: 444, full_page_write: false))
  end
end