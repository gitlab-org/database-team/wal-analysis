require 'spec_helper'

RSpec.describe WalAnalysis::Parser::Entry do
  let(:raw) do
    <<~RAW.gsub(/\n/, ' ').strip
      rmgr: Btree 
      len (rec/tot):     64/    64,
      tx: 2200386829,
      lsn: DB652/151B4C18,
      prev DB652/151B4BD8,
      desc: INSERT_LEAF off 33,
      blkref #0: rel 1663/16406/75581367 blk 34343
    RAW
  end

  it 'parses' do
    expect(described_class.parse(raw))
      .to eq(described_class.new(rmgr: 'Btree',
                                 rec_len: 64,
                                 tot_len: 64,
                                 description: 'INSERT_LEAF',
                                 block_refs: [WalAnalysis::Parser::BlkRef.new(location: WalAnalysis::Location.new(tablespace_oid: 1663, database_oid: 16406, relfilenode: 75581367), block: 34343, full_page_write: false)],
                                 ))
  end
end