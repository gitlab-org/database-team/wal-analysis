require 'spec_helper'

RSpec.describe WalAnalysis::Parser do
  it 'parses the fixture file' do
    with_file_fixture('wal_sample.txt') do |file|
      file.each do |line|
        expect { described_class.parse_line(line) }.not_to raise_error
      end
    end
  end
end