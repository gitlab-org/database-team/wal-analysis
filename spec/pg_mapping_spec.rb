# frozen_string_literal: true

require 'spec_helper'

RSpec.describe WalAnalysis::PgMapping do

  let(:mapping) do
    with_file_fixture('pg_mapping.csv') do |f|
      described_class.new(f)
    end
  end

  it 'loads the file' do
    mapping
  end

  it 'looks up a relation by location' do
    location = WalAnalysis::Location.new(tablespace_oid: 1663, database_oid: 524664, relfilenode: 525426)
    expect(mapping[location]).to eq('users')
  end
end