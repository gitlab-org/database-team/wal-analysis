select coalesce(nullif(pg_class.reltablespace, 0), pg_database.dattablespace) as tablespace_oid,
    pg_database.oid as database_oid,
    pg_class.relfilenode as relfilenode,
    pg_class.relname
from pg_class
         inner join pg_database on pg_database.datname = current_database()
where pg_class.relfilenode != 0;