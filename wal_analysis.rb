# frozen_string_literal: true

require_relative 'wal_analysis/location'
require_relative 'wal_analysis/parser'
require_relative 'wal_analysis/analysis'
require_relative 'wal_analysis/output_printer'
require_relative 'wal_analysis/pg_mapping'
require_relative 'wal_analysis/table_for_index'
require_relative 'wal_analysis/arguments'
require_relative 'wal_analysis/util'
require_relative 'wal_analysis/table_printer'

module WalAnalysis

  # TODO: Get access to benchmarking env again
  # Get a reference for relations so I can figure out what blkrefs refer to
  # correlate timestamps with the stream somehow and look for checkpoints


  def self.analyze(arguments)
    parser = Parser::LazyParser.new(arguments.input_file)
    mapping_file = File.join(File.expand_path(File.dirname(__FILE__)), 'data', 'oids_to_relnames.csv')
    mapping = PgMapping.new(mapping_file)
    table_for_index_file = File.join(File.expand_path(File.dirname(__FILE__)), 'data', 'indexes_to_table_names.csv')
    table_for_index = TableForIndex.new(table_for_index_file)
    # TODO: mapping is called relname_for_location on the other side, that's very confusing
    report = Analysis.new(parser, arguments, mapping, table_for_index).send(arguments.operation.to_sym, arguments.operation_argument)
    report.print
  end
end

# Huge hack to run analysis when this is called as a script but not when loaded during testing.
# Seemed easier than pulling in a dependency like thor.
unless defined?(RSpec)
  WalAnalysis.analyze(WalAnalysis::Arguments.parse!)
end