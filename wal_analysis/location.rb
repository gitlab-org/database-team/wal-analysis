module WalAnalysis
  class Location
    attr_reader :tablespace_oid, :database_oid, :relfilenode
    def initialize(tablespace_oid:, database_oid:, relfilenode:)
      unless tablespace_oid.is_a?(Integer) && database_oid.is_a?(Integer) && relfilenode.is_a?(Integer)
        raise ArgumentError, "arguments must be integers"
      end
      @tablespace_oid = tablespace_oid
      @database_oid = database_oid
      @relfilenode = relfilenode
    end

    def hash
      [tablespace_oid, database_oid, relfilenode].hash
    end

    def ==(other)
      other.is_a?(Location) &&
        other.tablespace_oid == tablespace_oid &&
        other.database_oid == database_oid &&
        other.relfilenode == relfilenode
    end

    alias_method :eql?, :==
    def to_s
      "#{tablespace_oid}/#{database_oid}/#{relfilenode}"
    end
  end
end