require 'optparse'

module WalAnalysis
  Arguments = Struct.new(
    :input_file,
    :skip_vacuums,
    :operation,
    :operation_argument,
    keyword_init: true
    ) do
    def self.parse!(raw_arguments = ARGV)
      chosen_options = Arguments.new(
        input_file: STDIN,
        skip_vacuums: false,
        operation: :top_writes_per_relation,
        operation_argument: ''
      )
      OptionParser.new do |opts|
        opts.default_argv = raw_arguments # So we can switch out arguments in testing

        opts.on('--skip-vacuums') do
          chosen_options.skip_vacuums = true
        end
        opts.on('--operation=operation') do |op|
          chosen_options.operation = op.to_sym
        end
        opts.on('--operation-argument=argument') do |op_arg|
          case chosen_options.operation
          when :top_writes_per_relation
            chosen_options.operation_argument = op_arg.to_i
          when :top_writes_isolate_by_relation
            chosen_options.operation_argument = Regexp.new(op_arg)
          else
            nil
          end
        end
      end.parse!(raw_arguments)

      # OptParse doesn't handle positional args, it leaves them in the raw array
      chosen_options.input_file = File.open(raw_arguments.shift) unless raw_arguments.empty?

      chosen_options
    end
  end
end