module WalAnalysis
  module Util
    def self.pretty_size_name(size_bytes)
      if size_bytes >= 10_000_000_000
        "#{(size_bytes.to_f / 1_000_000_000).round(4)} GB"
      elsif size_bytes >= 10_000_000
        "#{(size_bytes.to_f / 1_000_000).round(4)} MB"
      elsif size_bytes >= 10_000
        "#{(size_bytes.to_f / 1_000).round(4)} KB"
      else
        "#{size_bytes.to_f.round(4)} B"
      end
    end

    def self.pretty_percent(f)
      "#{(f * 100).round(2)}%"
    end
  end
end