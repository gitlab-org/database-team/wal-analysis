module WalAnalysis
  # Maps between physical locations and their database objects.
  # Regenerate the file by running scripts/pg_mapping.sql with --csv
  class TableForIndex
    extend Forwardable

    def initialize(mapping_file)
      load_file(mapping_file)
    end

    def_delegator :@mapping, :[]

    private

    def load_file(mapping_file)
      @mapping =  CSV.foreach(mapping_file, headers: true).to_h do |row|
        [row['indexname'], row['tablename']]
      end
    end
  end

end