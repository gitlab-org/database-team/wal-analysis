require 'stringio'

module WalAnalysis
  class OutputPrinter
    def initialize(*args)
      @buffer = StringIO.new
      @ready_to_print = false
    end

    def add_to_buffer
      yield @buffer
    end

    def finalize
      @ready_to_print = true
      return self
    end

    def print
      raise "Must call finalize before printing" unless @ready_to_print
      puts @buffer.string
    end

    def print_to(stringio)
      raise "Must call finalize before printing" unless @ready_to_print
      stringio.puts @buffer.string
    end
  end
end