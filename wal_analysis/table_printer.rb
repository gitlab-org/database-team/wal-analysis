module WalAnalysis
  class TablePrinter < OutputPrinter
    def initialize(headers)
      @headers = headers
      @widths = headers.map { |h| [h, 0] }.to_h
      @rows = []
      super
    end

    def add_row(row)
      row.each do |k, v|
        @widths[k] = [@widths[k], v.length].max
      end
      @rows << row
    end

    def finalize
      header_row = @headers.map { |h| [h, h.to_s] }.to_h
      print_row(header_row)
      # TODO: Figure out why line doesn't span full width
      line = '-' * (@widths.values.sum + @widths.count - 1)
      @buffer.puts(line)
      @rows.each { |r| print_row(r) }
      super
    end

    private

    def print_row(row)
      rendered_row = row.map do |k, v|
        v.ljust(@widths[k])
      end.join(' | ')

      @buffer.puts rendered_row
    end
  end
end