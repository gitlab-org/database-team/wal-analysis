require 'csv'

module WalAnalysis

  # Maps between physical locations and their database objects.
  # Regenerate the file by running scripts/pg_mapping.sql with --csv
  class PgMapping
    extend Forwardable

    def initialize(mapping_file)
      load_file(mapping_file)
    end

    def_delegator :@mapping, :[]

    private

    def load_file(mapping_file)
      @mapping =  CSV.foreach(mapping_file, headers: true).to_h do |row|
        # parse each location, build a hash of location -> name, use that in the analysis.
        location = WalAnalysis::Location.new(tablespace_oid: row['tablespace_oid'].to_i,
                                       database_oid: row['database_oid'].to_i,
                                       relfilenode: row['relfilenode'].to_i)
        [location, row['relname']]
      end
    end
  end
end