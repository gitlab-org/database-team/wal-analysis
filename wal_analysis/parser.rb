# frozen_string_literal: true

require_relative 'parser/entry'
require_relative 'parser/blkref'
require_relative 'parser/commit'
require_relative 'parser/transaction'
require_relative 'parser/unparsed'

module WalAnalysis
  module Parser
    LINE_MATCHER = /^rmgr: (\w+)/
    class << self
      def parse_line(line)
        Entry.parse(line)
      end

      def rmgr(line)
        match = line.match LINE_MATCHER
        match[1]
      end
    end

    class LazyParser
      include Enumerable

      def initialize(file)
        @file = file
      end

      def each
        @file.each do |line|
          yield Entry.parse(line)
        end
      end
    end
  end
end