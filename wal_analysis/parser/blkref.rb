module WalAnalysis
  module Parser
    class BlkRef

      LINE_MATCHER = /^blkref #\d+: rel (?<tablespace_oid>\d+)\/(?<database_oid>\d+)\/(?<relfilenode>\d+) (fork \w+ )?blk (?<block>\d+)(?<fpw> FPW)?$/

      attr_reader :location, :block, :full_page_write
      def initialize(location:, block:, full_page_write:)
        @location = location
        @block = block
        @full_page_write = full_page_write
      end
      def self.parse(snippet)
        m = snippet.match LINE_MATCHER
        new(
          location: WalAnalysis::Location.new(
            tablespace_oid: m[:tablespace_oid].to_i,
            database_oid: m[:database_oid].to_i,
            relfilenode: m[:relfilenode].to_i
          ),
          block: m['block'].to_i,
          full_page_write: !!m[:fpw]
        )
      end

      def ==(other)
        other.is_a?(BlkRef) &&
          location == other.location &&
          block == other.block &&
          full_page_write == other.full_page_write
      end
    end
  end
end