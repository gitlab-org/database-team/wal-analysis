module WalAnalysis
  module Parser
    class Transaction
      LINE_MATCHER = /rmgr: Transaction.*?desc: (?<desc>\w+).*?$/

      def self.parse(line)
        match = line.match LINE_MATCHER
        raise ArgumentError, "Unable to parse #{line}" unless match

        case match[:desc]
        when 'COMMIT'
          Commit.parse(line)
        else # e.g. INVALIDATION or other types of commit entries
          Unparsed.parse(line)
        end
      end
    end
  end
end

