module WalAnalysis
  module Parser
    class Commit

      LINE_MATCHER = /rmgr: Transaction.*?desc: COMMIT (?<timestamp>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{6}) UTC/

      attr_reader :timestamp
      def initialize(timestamp:)
        @timestamp = timestamp
      end
      def self.parse(line)
        # rmgr: Transaction len (rec/tot):     46/    46, tx: 2200386854, lsn: DB652/159B5690, prev DB652/159B3858, desc: COMMIT 2023-12-10 00:59:40.401732 UTC
        match = line.match LINE_MATCHER
        raise ArgumentError, "Unable to parse #{line}" unless match

        timestamp = DateTime.parse(match[:timestamp])
        new(timestamp: timestamp)
      end
    end
  end
end