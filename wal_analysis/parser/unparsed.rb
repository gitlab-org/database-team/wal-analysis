
module WalAnalysis
  module Parser
    Unparsed = Struct.new(:line, keyword_init: true) do
      def self.parse(line)
        new(line: line)
      end
    end
  end
end