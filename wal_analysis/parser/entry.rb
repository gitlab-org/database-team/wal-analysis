
module WalAnalysis
  module Parser
    class Entry
      attr_reader :rmgr, :rec_len, :tot_len, :description, :block_refs
      LINE_MATCHER = /^rmgr: (?<rmgr>\w+)\s*len\s*\(rec\/tot\):\s*(?<rec_len>\d+)\/\s*(?<tot_len>\d+), .*?desc: (?<desc>\w+).*?(?<blkrefs>blkref.*blk \d+( FPW)?)?$/

      def initialize(rmgr:, rec_len:, tot_len:, description:, block_refs:)
        @rmgr = rmgr
        @rec_len = rec_len
        @tot_len = tot_len
        @description = description
        @block_refs = block_refs
      end
      def self.parse(line)
        match = line.match LINE_MATCHER
        raise ArgumentError, "Unable to parse #{line}" unless match

        # This is a hack, commits are the only type we care about that don't match the blkref format.
        # The code to determine if it's a commit record type should live somewhere above Entry.parse()
        if match[:rmgr] == 'Transaction'
          return Transaction.parse(line)
        end

        new(
          rmgr: match[:rmgr],
          rec_len: match[:rec_len].to_i,
          tot_len: match[:tot_len].to_i,
          description: match[:desc],
          block_refs: match[:blkrefs]&.split(', ')&.map { |raw_ref| BlkRef.parse(raw_ref) } || [] # Transaction commits don't have any refs
        )
      end

      def ==(other)
        other.is_a?(Entry) &&
          other.block_refs == block_refs &&
          other.rmgr == rmgr &&
          other.description == description &&
          other.rec_len == rec_len &&
          other.tot_len == tot_len
      end
    end
  end
end