module WalAnalysis
  class Analysis
    def initialize(entries, arguments, relname_for_location, table_for_index)
      @entries = entries
      @arguments = arguments
      @relname_for_location = relname_for_location
      @table_for_index = table_for_index
    end

    def top_writes_per_relation_data(top_n)
      write_counts = {}
      write_counts_til_last_commit = nil
      entries = @entries.lazy

      start_commit = nil
      end_commit = nil
      entries.each do |entry|
        # Skip records that we don't care about (such as commit invalidations)
        next if entry.is_a?(WalAnalysis::Parser::Unparsed)

        # Begin by skipping until we find the first commit (which has timestamp info)
        if start_commit.nil?
          if entry.is_a?(WalAnalysis::Parser::Commit)
            start_commit = entry
          end
          next
        end

        # Record each further commit as a potential last commit, and stamp out the data collected to this point
        # We'll need to throw out data after the last commit so this lets us rewind to it
        if entry.is_a?(WalAnalysis::Parser::Commit)
          end_commit = entry
          write_counts_til_last_commit = write_counts
          write_counts = write_counts_til_last_commit.dup
          next
        end

        location = entry.block_refs.first&.location
        next if location.nil? # TODO: handle wal records that don't belong to a relation

        # Blkrefs are split across multiple relations, I hope this never happens
        if entry.block_refs.any? { |blkref| blkref.location != location }
          # TODO handle wal records that belong to multiple relations
          raise ArgumentError, "Unsupported: blkrefs split across tables: #{entry.blkrefs}"
        end
        write_counts[location] ||= {rec: 0, tot: 0}
        # TODO: clean up logic here
        should_count_write = true
        should_count_write = false if @arguments.skip_vacuums && entry.description == 'VACUUM'
        # TODO: Using :rec and :tot here instead of a real object is really hard to read, fix that.
        write_counts[location][:rec] += entry.rec_len if should_count_write
        write_counts[location][:tot] += entry.tot_len if should_count_write
      end

      # datetime difference gives fractional days, we want seconds
      duration = (end_commit.timestamp - start_commit.timestamp) * (24 * 60 * 60)

      # location -> write_count
      write_counts_by_relation = Hash.new { |h, k| h[k] = {rec: 0, tot: 0} }

      write_counts_til_last_commit.each do |location, write_count|
        if (relname = @relname_for_location[location])
          if (table_name = @table_for_index[relname])
            write_counts_by_relation[table_name][:rec] += write_count[:rec]
            write_counts_by_relation[table_name][:tot] += write_count[:tot]
          else
            write_counts_by_relation[relname][:rec] += write_count[:rec]
            write_counts_by_relation[relname][:tot] += write_count[:tot]
          end
        else
          write_counts_by_relation['All unknown relations'][:rec] += write_count[:rec]
          write_counts_by_relation['All unknown relations'][:tot] += write_count[:tot]
        end
      end
      rows = write_counts_by_relation.to_a.sort_by { |loc, count| -count[:tot] }
      rows = top_n > 1 ? rows.first(top_n) : rows
      rows.map { |loc, count| [loc, {rec: count[:rec] / duration.to_f, tot: count[:tot] / duration.to_f } ] }
    end

    def top_writes_isolate_by_relation(matching_expression)
      # TODO: This method is a mess, clean it up
      data = top_writes_per_relation_data(0)
      sum_base = data.map { |(k, v)| v[:rec] }.sum
      sum_total = data.map { |(k, v)| v[:tot] }.sum
      selected_tables = data.select { |(k, v)| k.match(matching_expression) }
      rejected_tables = data.reject { |(k, v)| k.match(matching_expression) }
      printer = OutputPrinter.new
      printer.add_to_buffer do |buf|
        selected_tables_sum = 0.0
        buf.puts "\nSelected Tables\n#{ '=' * 80 }"
        table_buf = TablePrinter.new([:name, :wal_rate_total, :percentage_of_total])
        selected_tables.each do |(k, v)|
          table_buf.add_row prettify_isolate_row(k, v[:tot], v[:tot] / sum_total)
          selected_tables_sum += v[:tot]
        end
        table_buf.finalize.print_to(buf)
        buf.puts "\n\n\nRejected Tables\n#{ '=' * 80 }"
        table_buf = TablePrinter.new([:name, :wal_rate_total, :percentage_of_total])
        rejected_tables.each do |(k, v)|
          table_buf.add_row prettify_isolate_row(k, v[:tot], v[:tot] / sum_total)
        end
        table_buf.finalize.print_to(buf)
        selected_tables_proportion = selected_tables_sum / sum_total
        buf.puts "\nSUMMARY\n"
        buf.puts "Analyzed #{data.length} tables."
        buf.puts "Selected tables' WAL traffic:  #{Util.pretty_size_name selected_tables_sum}/s"
        buf.puts "Total tables' WAL traffic:     #{Util.pretty_size_name sum_total}/s"
        buf.puts "Selected tables' proportion of traffic:     #{Util.pretty_percent selected_tables_proportion}"
      end
      printer.finalize
    end

    def top_writes_per_relation(n)
      data = top_writes_per_relation_data(n)
        .map { |(k, v)| prettify_wal_row(k, v[:rec], v[:tot]) }
      printer = TablePrinter.new([:name, :wal_rate_base, :wal_rate_total])
      data.each { |r| printer.add_row(r) }
      printer.finalize
    end

    def repeated_full_page_writes(_arg)
      wal_processed_bytes = 0
      needed_fpws = 0
      unneeded_fpws = 0
      wal_cutoff = 1_000_000_000 * 17 * 3 # 3 checkpoints wide
      seen_times = Hash.new { |h, k| h[k] = -Float::INFINITY }
      @entries.lazy.each do |entry|
        next if entry.is_a?(WalAnalysis::Parser::Commit) || entry.is_a?(WalAnalysis::Parser::Unparsed)
        wal_processed_bytes += entry.tot_len
        entry.block_refs.each do |ref|
          next unless ref.full_page_write

          prev_seen_time = seen_times[[ref.location, ref.block]]
          bytes_since_last_seen =  wal_processed_bytes - prev_seen_time
          if bytes_since_last_seen < wal_cutoff
            unneeded_fpws += 1
          else
            needed_fpws += 1
          end
          seen_times[[ref.location, ref.block]] = wal_processed_bytes
        end
      end

      printer = TablePrinter.new([:needed_fpws, :unneeded_fpws])
      printer.add_row({needed_fpws: needed_fpws.to_s, unneeded_fpws: unneeded_fpws.to_s})
      printer.finalize
      printer
    end

    private

    def prettify_isolate_row(name, wal_rate_total, percentage_of_total)
      {
        name: name,
        wal_rate_total: "#{Util.pretty_size_name(wal_rate_total)}/s",
        percentage_of_total: Util.pretty_percent(percentage_of_total)
      }
    end

    def prettify_wal_row(name, wal_rate_base, wal_rate_total)
      {
        name: name,
        wal_rate_base: "#{Util.pretty_size_name(wal_rate_base)}/s",
        wal_rate_total: "#{Util.pretty_size_name(wal_rate_total)}/s"
      }
    end
  end
end