## Running The Script

Make a shell script like this:

```shell
#! /usr/bin/env bash
/usr/lib/postgresql/14/bin/pg_waldump /var/opt/gitlab/wal_restore/00000002000F9814000000AC /var/opt/gitlab/wal_restore/00000002000F9814000000AF "$@"
```

Then run:

```shell
./run.sh | /path/to/ruby /path/to/script
```

The idea is that we're piping the output of `pg_waldump` into the `STDIN` of the Ruby process.

## Regenerating the object name mapping files

This requires a DB lab instance and the scripts in the `scripts/` folder. In the `wal-analysis` root directory:

```shell
psql "[YOUR CONNSTRING]" -f scripts/oids_to_relnames.sql --csv > oids_to_relnames.csv

psql "[YOUR CONNSTRING]" -f scripts/indexes_to_table_names.sql --csv > indexes_to_table_names.csv
```